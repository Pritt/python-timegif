# README #

### Requirements ###
* Python 2.7+
* Pillow compiled with fonttypes support - https://www.lfd.uci.edu/~gohlke/pythonlibs/ - Pillow‑4.3.0‑cp27‑cp27m‑win32.whl
* DateUtil - https://pypi.python.org/pypi/python-dateutil/2.6.1

### What is this? ###

Basically, I saw some websites could create a countdown timer that could be attached to an e-mail.
Thus I decided to investigate how it's done. It was a simple gif since no JavaScript code can be implemented inside an e-mail.
Images however are different story.
This led me to code up real quickly, as you can probably tell by reading my code, to create a simple script to take current time and subtract the future time and output it as gif.

![Alt Black background with white digits counting down time](https://imgur.com/dx2upbq.gif)

### What's going on in the code? ###
The timeUtil makes it a breeze for us to get the current date and the difference to the future date.
I've used Pillow library for Python to create an empty image, this was done with template image before but for the sake of optimizing got cut to coding the image.
Additionally the image is in RGBa since having it black and white increases the size and decreases the performance, quite odd but I'll have to investigate that later on.
Once that's done, I've created a loop with range of 90, those are the total frames going into the gif.
Then comes the countdown time that we put into the image copy, since we do not want to apply this text to the original image.
After that we draw the time awkwardly and append each image to a list.

Finally, we select the first image as the starting point and save it as a gif file with rest of the images appended there after. Duration enables us to add time between each frame (1000ms) and we get a countdown!


### What's next? ###

1. Reusable code
Remove the hardcoded time difference within the loop and format the string properly.

2. API request
Add this to live server to serve image to a request. With timezone support.

3. Optimize the application
Currently the serving time is far too slow to instantly get the timer. This ends up with countdown not being precise.

4. Properly display time
Single digit time is shown in, well single digits. Looks more like a number coutdown than time countdown.