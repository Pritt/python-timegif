'''
Extensions used:
https://pypi.python.org/pypi/python-dateutil/2.6.1
Pillow compiled with fonttypes

'''
from PIL import ImageFont, ImageDraw, Image
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta

currentDate = datetime.now()
#futureDate = currentDate + timedelta(days=2, minutes=20)
futureDate = datetime(2017, 12, 5, 5, 20)
difference = relativedelta(futureDate, currentDate)
#days = difference.days
#hours = difference.hours
#minutes = difference.minutes
#seconds = difference.seconds

image_list = []
image = Image.new('RGBa', (400, 80), color=0)
font = ImageFont.truetype('calibri.ttf', 14)
for i in range(90):
    if(difference.seconds == 0):
        difference.minutes = difference.minutes - 1
        difference.seconds = 60
    difference.seconds = difference.seconds - 1
    imagecopy = image.copy()
    draw = ImageDraw.Draw(imagecopy)
    time = 'Days: ' + str(difference.days) + ' Hours: ' + str(difference.days) + ' Minutes: ' + str(difference.minutes) + ' Seconds: '+ str(difference.seconds)
    draw.text((35, 20), str(time), (255, 0, 0), font=font)
    image_list.append(imagecopy)

image_list[0].save('countdown.gif', save_all=True, append_images=image_list[1:], duration=1000, optimize=True)
